package com.grace.nycschools.data.rest;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

import com.grace.nycschools.data.model.HighSchool;
import com.grace.nycschools.data.model.SATResult;

public class RepoRepository {

    private final RepoService repoService;

    @Inject
    public RepoRepository(RepoService repoService) {
        this.repoService = repoService;
    }

    public Single<List<HighSchool>> getHighSchools() {
        return repoService.getHighSchools();
    }

    public Single<List<SATResult>> getSATResult(String dbn) {
        return repoService.getSATResults(dbn);
    }

}
