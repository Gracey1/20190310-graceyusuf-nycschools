package com.grace.nycschools.data.model;

import com.google.gson.annotations.SerializedName;

public class SATResult {
    @SerializedName("num_of_sat_test_takers")
    public final String numTestTakers;
    @SerializedName("sat_critical_reading_avg_score")
    public final String avgReadingScore;
    @SerializedName("sat_math_avg_score")
    public final String avgMathScore;
    @SerializedName("sat_writing_avg_score")
    public final String avgWritingScore;

    public SATResult(String numTestTakers, String avgMathScore,String avgReadingScore, String avgWritingScore) {
        this.numTestTakers = numTestTakers;
        this.avgMathScore = avgMathScore;
        this.avgReadingScore = avgReadingScore;
        this.avgWritingScore = avgWritingScore;
    }
}
