package com.grace.nycschools.data.model;

import com.google.gson.annotations.SerializedName;

public class HighSchool {
    public final String dbn;
    public final String borough;
    public final String neighborhood;
    public final String location;
    @SerializedName("overview_paragraph")
    public final String overviewParagraph;
    @SerializedName("phone_number")
    public final String phoneNumber;
    @SerializedName("school_name")
    public final String schoolName;
    public final String website;

    public HighSchool(String dbn, String schoolName,String overviewParagraph, String location, String neighborhood,
                      String borough, String phoneNumber, String website) {
        this.dbn = dbn;
        this.schoolName = schoolName;
        this.overviewParagraph = overviewParagraph;
        this.location = location;
        this.neighborhood = neighborhood;
        this.borough = borough;
        this.phoneNumber = phoneNumber;
        this.website = website;
    }
}
