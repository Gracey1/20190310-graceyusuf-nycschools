package com.grace.nycschools.data.rest;

import java.util.List;

import io.reactivex.Single;

import com.grace.nycschools.data.model.HighSchool;
import com.grace.nycschools.data.model.SATResult;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RepoService {

    @GET("s3k6-pzi2.json")
    Single<List<HighSchool>> getHighSchools();

    @GET("f9bf-2cp4.json")
    Single<List<SATResult>> getSATResults(@Query("dbn") String dbn);
}
