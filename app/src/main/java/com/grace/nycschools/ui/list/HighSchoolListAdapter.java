package com.grace.nycschools.ui.list;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grace.nycschools.R;
import com.grace.nycschools.data.model.HighSchool;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HighSchoolListAdapter extends RecyclerView.Adapter<HighSchoolListAdapter.ViewHolder>{

    private HighSchoolSelectedListener highSchoolSelectedListener;
    private final List<HighSchool> data = new ArrayList<>();

    HighSchoolListAdapter(ListViewModel viewModel, LifecycleOwner lifecycleOwner, HighSchoolSelectedListener highSchoolSelectedListener) {
        this.highSchoolSelectedListener = highSchoolSelectedListener;
        viewModel.getHighSchools().observe(lifecycleOwner, repos -> {
            data.clear();
            if (repos != null) {
                data.addAll(repos);
                notifyDataSetChanged();
            }
        });
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_repo_list_item, parent, false);
        return new ViewHolder(view, highSchoolSelectedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    static final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewSchoolName) TextView textViewSchoolName;

        private HighSchool highSchool;

        ViewHolder(View itemView, HighSchoolSelectedListener highSchoolSelectedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(highSchool != null) {
                    highSchoolSelectedListener.onHighSchoolSelected(highSchool);
                }
            });
        }

        void bind(HighSchool highSchool) {
            this.highSchool = highSchool;
            textViewSchoolName.setText(highSchool.schoolName);

        }
    }
}
