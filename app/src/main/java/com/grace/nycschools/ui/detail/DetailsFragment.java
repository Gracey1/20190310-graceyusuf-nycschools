package com.grace.nycschools.ui.detail;

import android.os.Bundle;

import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;


import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;

import com.grace.nycschools.R;
import com.grace.nycschools.base.BaseFragment;
import com.grace.nycschools.util.ViewModelFactory;

public class DetailsFragment extends BaseFragment {

    @BindView(R.id.textViewSchoolName)
    TextView textViewSchoolName;
    @BindView(R.id.textViewOverviewParagraph)
    TextView textViewOverviewParagraph;
    @BindView(R.id.textViewLocation)
    TextView textViewLocation;
    @BindView(R.id.textViewPhoneNumber)
    TextView textViewPhoneNumber;
    @BindView(R.id.textViewNeighborhood)
    TextView textViewNeighborhood;
    @BindView(R.id.textViewBorough)
    TextView textViewBorough;
    @BindView(R.id.textViewWebsite)
    TextView textViewWebsite;

    @BindView(R.id.textViewNumTestTakers)
    TextView textViewNumTestTakers;
    @BindView(R.id.textViewAvgMathScore)
    TextView textViewAvgMathScore;
    @BindView(R.id.textViewAvgReadingScore)
    TextView textViewAvgReadingScore;
    @BindView(R.id.textViewAvgWritingScore)
    TextView textViewAvgWritingScore;

    @BindView(R.id.tv_error) TextView errorTextView;
    @BindView(R.id.tableSATScores) View tableView;

    @Inject
    ViewModelFactory viewModelFactory;
    private DetailsViewModel viewModel;

    @Override
    protected int layoutRes() {
        return R.layout.screen_details;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(DetailsViewModel.class);
        subscribeToViewModel();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void subscribeToViewModel() {

        //Future Enhancements - Use Databinding via xml to ViewModel and reduce this boilerplate code
        //Future Enhancements - Inject ResourceManager for String access

        viewModel.getSelectedRepo().observe(this, highSchool -> {
            if (highSchool != null) {
                //clear previous values
                textViewNumTestTakers.setText("");
                textViewAvgMathScore.setText("");
                textViewAvgReadingScore.setText("");
                textViewAvgWritingScore.setText("");

                viewModel.fetchSATResult(highSchool.dbn);
                textViewSchoolName.setText(highSchool.schoolName);
                textViewOverviewParagraph.setText(highSchool.overviewParagraph);
                textViewLocation.setText(getString(R.string.location, highSchool.location));
                textViewPhoneNumber.setText(getString(R.string.phoneNumber, highSchool.phoneNumber));
                textViewNeighborhood.setText(getString(R.string.neighborhood, highSchool.neighborhood));
                textViewBorough.setText(getString(R.string.borough, highSchool.borough));
                textViewWebsite.setText(getString(R.string.website, highSchool.website));



            }
        });

        viewModel.getResults().observe(this, results -> {
            if (results != null) {
                textViewNumTestTakers.setText(getString(R.string.num_of_test_takers, results.get(0).numTestTakers));
                textViewAvgMathScore.setText(results.get(0).avgMathScore);
                textViewAvgReadingScore.setText(results.get(0).avgReadingScore);
                textViewAvgWritingScore.setText(results.get(0).avgWritingScore);

            }
        });

        //Future Enhancements - Display progress UI or Errors for SAT results

    }
}
