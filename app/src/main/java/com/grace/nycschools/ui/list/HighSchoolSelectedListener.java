package com.grace.nycschools.ui.list;

import com.grace.nycschools.data.model.HighSchool;

public interface HighSchoolSelectedListener {

    void onHighSchoolSelected(HighSchool highSchool);
}
