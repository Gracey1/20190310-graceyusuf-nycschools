package com.grace.nycschools.ui.detail;


import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import com.grace.nycschools.data.model.HighSchool;
import com.grace.nycschools.data.model.SATResult;
import com.grace.nycschools.data.rest.RepoRepository;

import java.util.List;

public class DetailsViewModel extends ViewModel {

    private final RepoRepository repoRepository;
    private CompositeDisposable disposable;

    private final MutableLiveData<HighSchool> selectedHighSchool = new MutableLiveData<>();

    private final MutableLiveData<List<SATResult>> results = new MutableLiveData<>();
    //private final MutableLiveData<Boolean> loadError = new MutableLiveData<>();
    //private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    /*LiveData<Boolean> getError() {
        return loadError;
    }
    LiveData<Boolean> getLoading() {
        return loading;
    }*/
    LiveData<List<SATResult>> getResults() { return results;}

    public LiveData<HighSchool> getSelectedRepo() {
        return selectedHighSchool;
    }

    @Inject
    public DetailsViewModel(RepoRepository repoRepository) {
        this.repoRepository = repoRepository;

        //Future enhancement - Have one compositeDisposable for whole application and obtain via injection
        disposable = new CompositeDisposable();
    }

    public void setSelectedRepo(HighSchool highSchool) {
        selectedHighSchool.setValue(highSchool);
    }


    public void fetchSATResult(String dbn) {
        disposable.add(repoRepository.getSATResult(dbn)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<SATResult>>() {
                    @Override
                    public void onSuccess(List<SATResult> value) {
                        if(value != null && !value.isEmpty()){
                            results.setValue(value);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        //Future enhancements - Use MutableLiveData field here to update Ui for any errors
                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
