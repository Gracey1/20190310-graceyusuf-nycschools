package com.grace.nycschools.ui.list;


import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.grace.nycschools.R;
import com.grace.nycschools.base.BaseFragment;
import com.grace.nycschools.data.model.HighSchool;
import com.grace.nycschools.ui.detail.DetailsViewModel;
import com.grace.nycschools.util.ViewModelFactory;
import com.grace.nycschools.ui.detail.DetailsFragment;

public class ListFragment extends BaseFragment implements HighSchoolSelectedListener {

    @BindView(R.id.recyclerView) RecyclerView listView;
    @BindView(R.id.tv_error) TextView errorTextView;
    @BindView(R.id.loading_view) View loadingView;

    @Inject ViewModelFactory viewModelFactory;
    private ListViewModel viewModel;

    @Override
    protected int layoutRes() {
        return R.layout.screen_list;
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        viewModel  = ViewModelProviders.of(this, viewModelFactory).get(ListViewModel.class);

        listView.setAdapter(new HighSchoolListAdapter(viewModel, this, this));
        listView.setLayoutManager(new LinearLayoutManager(getContext()));
        listView.setHasFixedSize(true);

        subscribeToViewModel();
    }

    private void subscribeToViewModel() {
        viewModel.getHighSchools().observe(this, repos -> {
            if(repos != null) listView.setVisibility(View.VISIBLE);
        });

        viewModel.getError().observe(this, isError -> {
            if (isError != null) if(isError) {
                errorTextView.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                errorTextView.setText("An Error Occurred While Loading Data!");
            }else {
                errorTextView.setVisibility(View.GONE);
                errorTextView.setText(null);
            }
        });

        viewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                loadingView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    errorTextView.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onHighSchoolSelected(HighSchool highSchool) {
        DetailsViewModel detailsViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(DetailsViewModel.class);
        detailsViewModel.setSelectedRepo(highSchool);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.screenContainer, new DetailsFragment())
                .addToBackStack(null).commit();

    }
}
