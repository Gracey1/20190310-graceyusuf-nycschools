package com.grace.nycschools.ui.main;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import com.grace.nycschools.ui.detail.DetailsFragment;
import com.grace.nycschools.ui.list.ListFragment;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract ListFragment provideListFragment();

    @ContributesAndroidInjector
    abstract DetailsFragment provideDetailsFragment();
}
