package com.grace.nycschools.ui.main;

import android.os.Bundle;

import com.grace.nycschools.R;
import com.grace.nycschools.base.BaseActivity;
import com.grace.nycschools.ui.list.ListFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.screenContainer, new ListFragment()).commit();
    }
}
